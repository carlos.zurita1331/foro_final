// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'foro-chat-2fa10',
    appId: '1:376668547458:web:9a33231ab20b99b9f2c79b',
    storageBucket: 'foro-chat-2fa10.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyCnOHyu4T7nOX7L0ach_mTrFZe2orBEWDI',
    authDomain: 'foro-chat-2fa10.firebaseapp.com',
    messagingSenderId: '376668547458',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
