import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface PeriodicElement {
  Titulo: string;
  Posicion: number;
  Autor: string;
  fecha: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {Posicion: 1, Titulo: 'Eutanasia', Autor: 'Mario Alcoba', fecha: '23 de junio 2022'},
  {Posicion: 2, Titulo: 'El cambio climatico', Autor: 'Mario Alcoba', fecha: '12 de mayo 2022'},
  {Posicion: 4, Titulo: 'El uso de redes sociales', Autor:'Gabriel Zapata', fecha: '10 de marzo 2022'},
  {Posicion: 5, Titulo: 'la experimentacion animal', Autor: 'Royer Carrasco', fecha: '7 de enero de 2022'},
  
];
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  user$ = this.usersService.currentUserProfile$;
  
  displayedColumns: string[] = ['Posicion', 'Titulo', 'Autor', 'fecha'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  User$= this.authService.currentUser$
  constructor(private usersService: UsersService,
              private authService: AuthService,
              public dialog: MatDialog) {}

   ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {}
}
